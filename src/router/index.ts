import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ClientView from "../views/ClientView.vue";
import DealerView from "../views/DealerView.vue";
import ImporterView from "../views/ImporterView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/client",
      name: "client",
      component: ClientView,
    },
    {
      path: "/dealer",
      name: "dealer",
      component: DealerView,
    },
    {
      path: "/importer",
      name: "importer",
      component: ImporterView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
  ],
});

export default router;
